import requests
import json
from time import sleep

def fdrop(channelid, delay): # Drop card function. illiterate through the token file, sending "k!d" with each token.
    with open("tokens.dat") as f:
        count = 0
        for line in f:
            # format the token so it works with requests (remove \n)
            token = line.rstrip("\n")

            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            data = '{"content":"k!d", "tts":false}'

            response = requests.post(f'https://discord.com/api/v8/channels/{channelid}/messages', headers=headers, data=data)
            if delay == True:
                sleep(2)
            count +=1
            if count > 9:
                if count < 11:
                    print("Sleeping!")
                    sleep(60)

def fcooldown(channelid, delay): # Cooldown card function. illiterate through the token file, sending "k!cd" with each token.
    with open("tokens.dat") as f:
        count = 0
        for line in f:
            count += 1
            # format the token so it works with requests (remove \n)
            token = line.rstrip("\n")

            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            data = '{"content":"k!cd", "tts":false}'

            response = requests.post(f'https://discord.com/api/v8/channels/{channelid}/messages', headers=headers, data=data)
            if delay == True:
                sleep(2)
            if count > 3:
                return


def freact(channel, message, reaction): # Reaction function. Reacts to a message with the inputed message id with a set of predefined reactions.
    with open("tokens.dat") as f:
        for line in f:

            if reaction == "1":
                emote = "1️⃣"
            elif reaction == "2":
                emote = "2️⃣"
            elif reaction == "3":
                emote = "3️⃣"
            elif reaction == "4":
                emote = "4️⃣"
            elif reaction == "check":
                emote = "✅"

            url = f"https://discord.com/api/v8/channels/{channel}/messages/{message}/reactions/{emote}/%40me"

            token = line.rstrip("\n")
            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            response = requests.put(url, headers=headers)

            return

def ftransfer(channel, code, user): # Transfer card function. Sends a specific card to the user from the first token.
    with open("tokens.dat") as f:
        for line in f:
            # format the token so it works with requests (remove \n)
            token = line.rstrip("\n")

            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            data = {}
            data["content"] = f"k!give {user} {code}"
            data["tts"] = "false"
            body = json.dumps(data)

            response = requests.post(f'https://discord.com/api/v8/channels/{channel}/messages', headers=headers, data=body)
            return

def fexec(channel, command): # Custom command function. Allows the user to make the first user run any command.
    with open("tokens.dat") as f:
        for line in f:
            # format the token so it works with requests (remove \n)
            token = line.rstrip("\n")

            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            data = {}
            data["content"] = f"{command}"
            data["tts"] = "false"
            body = json.dumps(data)

            response = requests.post(f'https://discord.com/api/v8/channels/{channel}/messages', headers=headers, data=body)
            return

def fstats(): # Stats function. Shows how many tokens are in the tokens.dat file.
    with open("tokens.dat") as f:
        count = 0
        for line in f:
            count += 1
        return count

def fjoin(invite): # Mass joining functionality (Admin command)
     with open("tokens.dat") as f:
         for line in f:
             # format the token so it works with requests (remove \n)
             token = line.rstrip("\n")

             headers = {}
             headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
             headers["Accept"] = "*/*"
             headers["Accept-Language"] = "en-US"
             headers["Content-Type"] = "application/json"
             headers["Authorization"] = f"{token}"

             response = requests.post(f'https://discord.com/api/v8/invites/{invite}', headers=headers)

def fleave(guildid): # Mass leaving functionality (Admin command)
     with open("tokens.dat") as f:
         for line in f:
             # format the token so it works with requests (remove \n)
             token = line.rstrip("\n")

             headers = {}
             headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
             headers["Accept"] = "*/*"
             headers["Accept-Language"] = "en-US"
             headers["Content-Type"] = "application/json"
             headers["Authorization"] = f"{token}"

             response = requests.delete('https://discord.com/api/v8/users/@me/guilds/{guildid}', headers=headers)


def fmassreact(channel, message, reaction): # Reaction function. Reacts to a message with the inputed message id with a set of predefined reactions.
    with open("tokens.dat") as f:
        for line in f:
            if reaction == "1":
                emote = "1️⃣"
            elif reaction == "2":
                emote = "2️⃣"
            elif reaction == "3":
                emote = "3️⃣"
            elif reaction == "4":
                emote = "4️⃣"

            url = f"https://discord.com/api/v8/channels/{channel}/messages/{message}/reactions/{emote}/%40me"

            token = line.rstrip("\n")
            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            response = requests.put(url, headers=headers)

def fcollect(channelid): # Drop card function. illiterate through the token file, sending "k!d" with each token.
    with open("tokens.dat") as f:
        count = 0
        for line in f:
            # format the token so it works with requests (remove \n)
            token = line.rstrip("\n")

            headers = {}
            headers["User-Agent"] = 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
            headers["Accept"] = "*/*"
            headers["Accept-Language"] = "en-US"
            headers["Content-Type"] = "application/json"
            headers["Authorization"] = f"{token}"

            data = '{"content":"k!c", "tts":false}'

            response = requests.post(f'https://discord.com/api/v8/channels/{channelid}/messages', headers=headers, data=data)
            sleep(2)
