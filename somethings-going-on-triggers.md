List of things that can trigger the Something's going on screen on Discord

1. Attempting to send a server leave DELETE request

2. Attempting to send a friend requests with the API

3. Attempting to leave a server you're not in with the API
