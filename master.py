import discord
from discord.ext import commands
import requests
import json
import slave

TOKEN = ('')

bot = commands.Bot(command_prefix='##')

@bot.event
async def on_ready():
    print(f'{bot.user} has connected to the Discord API')

@bot.command(description="Drops cards")
async def fdrop(ctx, delay : str = True):
    await ctx.send("executing `forcedrop`")
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.fdrop(channel, delay)

@bot.command(description="Checks the clients' cooldowns")
async def fcheck(ctx, delay : str = True):
    await ctx.send("executing `forcecooldown`")
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.fcooldown(channel, delay)

@bot.command(description="React to a message, Available reactions are 1, 2, 3, 4, and check")
async def freact(ctx, message, reaction):
    await ctx.send(f"reacting to `{message}` with `{reaction}`")
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.freact(channel, message, reaction)

@bot.command(description="Transfer a card")
async def ftransfer(ctx, code):
    await ctx.send(f"transferring card `{code}` to {ctx.author.mention}...")
    user = ctx.author.mention
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.ftransfer(channel, code, user)

@bot.command(description="Execute a custom command")
async def fexec(ctx, *, cmd):
    await ctx.send(f"executing `{cmd}`")
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.fexec(channel, cmd)

@bot.command(description="Shows how many tokens exist")
async def fstats(ctx):
    count = slave.fstats()
    await ctx.send(f"`{count}` tokens on file.")

@bot.command(description="Mass joins (Bot Admin only)")
async def massjoin(ctx, invite):
    if ctx.author.id in {}:
        slave.fjoin(invite)
    else:
        await ctx.send("bot owner only")

@bot.command(description="Mass leaves (Bot Admin only)")
async def massleave(ctx, serverid):
    if ctx.author.id in {}:
        slave.fleave(serverid)
        await ctx.send("Retreated")
    else:
        await ctx.send("bot owner only")

@bot.command(description="React to a message, Available reactions are 1, 2, 3, 4, and check")
async def massreact(ctx, message, reaction):
    await ctx.send(f"reacting to `{message}` with `{reaction}`")
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.fmassreact(channel, message, reaction)

@bot.command(description="Mass checks every account's collection")
async def masscollect(ctx):
    await ctx.send("Mass checking collection")
    chid = ctx.message.channel.id
    channel = str(chid)
    slave.fcollect(channel)




bot.run(TOKEN)
